# FizzBuzzApplication

Fizz buzz is a group word game for children to teach them about division.[1] Players take turns to count incrementally, replacing any number divisible by three with the word "fizz", and any number divisible by five with the word "buzz" [1]

# Play
Program prints the integers from n to m (inclusive), but
*	for multiples of three, print Fizz (instead of the number)
*	for multiples of five, print Buzz (instead of the number)
* 	for multiples of both three and five, print FizzBuzz (instead of the number)

Input numbers need to satisfy condition 1 <= n < m <= 10000.

[1]: https://en.wikipedia.org/wiki/Fizz_buzz

## Input
Two numbers in two lines (n, m).

## Output
One result per line as described in requirements.

## Example
Sample input: 

3

16

Sample output: 

Fizz

4

Buzz

Fizz

7

8

Fizz

Buzz

11

Fizz

13

14

FizzBuzz

16
