def fizzbuzz(firstNum, lastNum):
    for i in range(firstNum, lastNum + 1):
        if i % 3 == 0 and i % 5 == 0:
            print('FizzBuzz')
        elif i % 3 == 0:
            print('Fizz')
        elif i % 5 == 0:
            print('Buzz')
        else:
            print(i)


print("Please type numbers in range <1,10000>, but the first should be smaller than the second: ")
while True:
    try:
        firstNum = int(input())
        break
    except ValueError:
        print("Value should be integer, please type value again")

while not (1 <= firstNum <= 9999):
    print("Please type value again in range <1,9999>")
    firstNum = int(input())

while True:
    try:
        lastNum = int(input())
        break
    except ValueError:
        print("Value should be integer, please type value again")

while not ((1 <= lastNum <= 10000) and (firstNum < lastNum)):
    print("Please type value again in range <1,10000>")
    lastNum = int(input())


fizzbuzz(firstNum, lastNum)

